package com.oa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@MapperScan("com.oa.dao")
@SpringBootApplication

public class OaThereApplication {

    public static void main(String[] args) {
        SpringApplication.run(OaThereApplication.class, args);
    }

}
