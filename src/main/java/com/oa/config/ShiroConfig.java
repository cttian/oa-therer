package com.oa.config;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import java.util.LinkedHashMap;
import java.util.Properties;


@Configuration
public class ShiroConfig {

	/**
	 * 解决setUnauthorizedUrl不能跳转的问题
	 */
	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
		authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
		return authorizationAttributeSourceAdvisor;
	}

	/**
	 * shiro的过滤器ShiroFilterFactoryBean，
	 */
	@Bean
	public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {

		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();

		// Shiro的核心安全接口
		shiroFilterFactoryBean.setSecurityManager(securityManager);

		//没有登录发送请求时设置跳转到登录
		shiroFilterFactoryBean.setLoginUrl("/test");

		//访问没有权限的接口跳转的接口
//		shiroFilterFactoryBean.setUnauthorizedUrl("/notPer");

		//自定义过滤链，使用LinkedHashMap保证有序
		LinkedHashMap<String, String> filterChainDefinitionMap = new LinkedHashMap<String, String>();

		/*
		 * 配置拦截规则，anon 表示资源都可以匿名访问
		 * authc：登录后能访问/*一层 /** 多层
		 */
		filterChainDefinitionMap.put("/public/**", "anon");
		filterChainDefinitionMap.put("/mySocket/{userId}", "anon"); //放开webscoket
		//其他资源都需要认证
		filterChainDefinitionMap.put("/*", "authc");
		shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);


		return shiroFilterFactoryBean;

	}

	/**
	 * Shiro的核心安全接口
	 */
	@Bean
	public SecurityManager securityManager() {

		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

		securityManager.setRealm(customRealm());


		return securityManager;

	}

	/**
	 * 自定义realm
	 */
	@Bean
	public CustomRealm customRealm() {

		return new CustomRealm();
	}



	@Bean
	public SimpleMappingExceptionResolver simpleMappingExceptionResolver() {
		SimpleMappingExceptionResolver simpleMappingExceptionResolver=new SimpleMappingExceptionResolver();
		Properties properties=new Properties();
		properties.setProperty("org.apache.shiro.authz.UnauthorizedException","/index/unauthorized");
		properties.setProperty("org.apache.shiro.authz.UnauthenticatedException","/index/unauthorized");
		simpleMappingExceptionResolver.setExceptionMappings(properties);
		return simpleMappingExceptionResolver;
	}


	@Bean
	public static DefaultAdvisorAutoProxyCreator getDefaultAdvisorAutoProxyCreator() {

		DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
		defaultAdvisorAutoProxyCreator.setUsePrefix(true);

		return defaultAdvisorAutoProxyCreator;
	}

	@Bean
	public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator(){
		DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
		advisorAutoProxyCreator.setProxyTargetClass(true);
		return advisorAutoProxyCreator;
	}
}
