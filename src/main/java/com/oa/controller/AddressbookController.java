package com.oa.controller;

import com.oa.config.Result;
import com.oa.entity.Addressbook;
import com.oa.entity.Users;
import com.oa.service.AddressbookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

//通讯录的控制层
@RestController
@RequestMapping("/Addressbook")
public class AddressbookController {
    @Autowired
    public AddressbookService addressbookService;

    @RequestMapping("/aAll")
    public Object aAll(@RequestParam Integer page, @RequestParam Integer limit , Addressbook li, @RequestParam Map<String,Object> map) {
        map.put("page", (page-1)*limit);
        map.put("limit", limit);
        map.put("abname", li.getAbname());
        return new Result("0","true",addressbookService.count(map),addressbookService.aAll(map));
    }
    @RequestMapping("/add")
    public Object add(@RequestParam Map<String, Object> map) {
        return new Result("0","0",addressbookService.add(map),true);

    }
    @RequestMapping("/update")
    public Object update(@RequestParam Map<String,Object> map) {

        return new Result("0","0",addressbookService.update(map),true);

    }
    @RequestMapping("/delete")
    public Object delete(int id) {

        return new Result("0","0",addressbookService.delete(id),true);

    }

}
