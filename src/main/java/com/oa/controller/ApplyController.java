package com.oa.controller;

import com.oa.dao.ApplyDao;
import com.oa.dao.BecomeDao;
import com.oa.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Mr.suho
 * @Date 2021/12/6 9:13
 * @Description ApplyController
 * @Version 1.0
 */
@RestController
    @RequestMapping("/apply")
public class ApplyController {
    @Autowired
    ApplyDao applyDao;
    @Autowired
    BecomeDao becomeDao;
    /*根据用户id查询所申请的信息*/
    @RequestMapping("getMyapplyByUserId")
    public Result getMyapplyByUserId(Integer id){

        Result result = new Result("","",20,applyDao.getMyapplyByUserId(id));
        return result;
    }
    /*将当前填写表单的用户id存入数据库*/
    @RequestMapping("/addMyapplyUserId")
    public Integer addMyapplyUserId(Integer userid){

        return applyDao.addMyapplyUserId(userid);
    }
}
