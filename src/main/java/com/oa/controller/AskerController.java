package com.oa.controller;

import com.oa.entity.Asker;
import com.oa.entity.Users;
import com.oa.service.AskerService;
import com.oa.service.UsersService;
import com.oa.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/asker")
public class AskerController {
	@Autowired
	private AskerService askerService;
	@Autowired
	private UsersService usersService;// 自动注入服务层

	/**
	 * 多条件查询签到表 多条件（用户名、签到时间、签到状态） 只能查看当天签到的
	 * 
	 * @param m
	 * @param page
	 * @param limit
	 * @return
	 */
	@RequestMapping("/getAllAsker")
	public Object getAllAsker(@RequestParam Map<String, Object> m, int page, int limit, String beginTime,
			String endTime, String checkInTime) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式 获取当天时间
		m.put("page", (page - 1) * limit);
		m.put("limit", limit);
		m.put("beginTime", beginTime);
		m.put("endTime", endTime);
		m.put("checkInTime", df.format(new Date()));
		Result result = new Result("0", "true", askerService.getAllAskerCount(m), askerService.getAllAsker(m));
		return result;
	}

	/**
	 * 签到
	 * 
	 * @param asker
	 * @return
	 */
	@RequestMapping("/add")
	public Object add(Users users, Asker asker, String askerName) {
		users.setLoginName(askerName);
//		System.out.println(usersService.getZiXunWeight(users));//根据咨询师名查询咨询师权重
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		asker.setAskerName(askerName);
		asker.setCheckState("已签到");
		asker.setCheckInTime(df.format(new Date()));
//		asker.setWeight(usersService.getZiXunWeight(users));// 添加权重
		Result result = new Result("0", "签到成功", askerService.add(asker), "后台请求成功");
		return result;
	}

	/**
	 * 据登录名查询是否该签到 >0签到
	 * 
	 * @param loginName
	 * @return
	 */
	@RequestMapping("/askerYesNo")
	public Object askerYesNo(String loginName) {
		Result result = new Result("0", "true", askerService.askerYesNo(loginName), "后台请求成功");
		return result;
	}

	/**
	 * 根据登录名查询今天是否签到，已签到不在显示签到按钮 >0一签到
	 * 
	 * @param loginName
	 * @return
	 */
	@RequestMapping("/askerYesNoAsker")
	public Object askerYesNoAsker(Asker asker) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
		asker.setCheckInTime(df.format(new Date()));
		Result result = new Result("0", "true", askerService.askerYesNoAsker(asker), "后台请求成功");
		return result;
	}

	/**
	 * 根据登录名查询今天是否签退，和签到，已签退，签到不在显示签退按钮 >0已经签退
	 * 
	 * @param loginName
	 * @return
	 */
	@RequestMapping("/askerOutTime")
	public Object askerOutTime(Asker asker) {
		SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
		asker.setCheckOutTime(dff.format(new Date()));
		Result result = new Result("0", "true", askerService.askerOutTime(asker), "后台请求成功");
		return result;
	}

	/**
	 * 根据登录名查询今天签到时间，判断今天有没有签到 >0已经签到
	 * 
	 * @param loginName
	 * @return
	 */
	@RequestMapping("/askerTime")
	public Object askerTime(Asker asker) {
		SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
		asker.setCheckInTime(dff.format(new Date()));
		Result result = new Result("0", "true", 0, askerService.askerTime(asker));
		return result;
	}

//	/**
//	 * 修改签到后咨询师的权重
//	 *
//	 * @param asker
//	 * @return
//	 */
//	@RequestMapping("/updateWeight")
//	public Object updateWeight(Asker asker) {
//		Result result = new Result("0", "true", askerService.updateWeight(asker), "后台请求成功");
//		return result;
//	}

	/**
	 * 签退,根据签到时间,签到人去签退,签退过的不能在签退了
	 * 
	 * @param asker
	 * @return
	 */
	@RequestMapping("/update")
	public Object update(Asker asker) {
		SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
		asker.setCheckInTime(dff.format(new Date()));
		String ss = askerService.askerTime(asker);// 根据登录人查询今天签到时间
		System.out.println(ss);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
//			df.format(new Date())当前系统时间
		try {
			Date currentDate = df.parse(df.format(new Date()));
			Date ssDate = df.parse(ss);
			System.out.println(currentDate);// 当前系统时间
			System.out.println(ssDate);// 签到时间
			Calendar cal = Calendar.getInstance();
			cal.setTime(ssDate);
			long time1 = cal.getTimeInMillis();// 将时间转换成毫秒
			cal.setTime(currentDate);
			long time2 = cal.getTimeInMillis();// 将时间转换成毫秒
			long between_days = (time2 - time1);// 时间之差毫秒
			if (between_days >= 600000) {// >10分钟签退
				Asker aske = new Asker();//重新建立一个Asker
				aske.setCheckState("已签退");
				aske.setCheckInTime(ss);
				aske.setCheckOutTime(df.format(new Date()));
				System.out.println(aske);
				Result result = new Result("0", "签退成功", askerService.update(aske), "后台请求成功");
				System.out.println(askerService.update(aske));
				return result;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Result result = new Result("0", "签到还未满10分钟，不允许签退", 0, "后台请求失败");
		return result;
	}
}
