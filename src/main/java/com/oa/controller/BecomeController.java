package com.oa.controller;

import com.oa.dao.ApplyDao;
import com.oa.dao.BecomeDao;
import com.oa.entity.BecomeForm;
import com.oa.util.Result;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * @Author Mr.suho
 * @Date 2021/12/5 16:57
 * @Description BecomeController
 * @Version 1.0
 */
@RestController
@RequestMapping("/become")
public class BecomeController {
    @Autowired
    BecomeDao becomeDao;
    @Autowired
    ApplyDao applyDao;
    /*存员工填写的，表单数据+id */
    @RequestMapping("/addBecomeForm/{userId}")
    public Integer add(BecomeForm becomeForm, @PathVariable("userId") String userId){
        Date date = new Date();
        becomeForm.setCreatetime(date);//存入创建时间
        becomeForm.setUserid(Integer.parseInt(userId));
        System.out.println(becomeForm);
       // applyDao.addMyapplyUserId(id);
        return becomeDao.addBecomeForm(becomeForm);
    }
}
