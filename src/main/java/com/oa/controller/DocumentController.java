package com.oa.controller;

import com.oa.config.Result;
import com.oa.entity.Addressbook;
import com.oa.entity.Document;
import com.oa.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.xml.ws.Action;
import java.util.Map;

//文档的控制层
@RestController
@RequestMapping("/Document")
public class DocumentController {
    @Autowired
    DocumentService documentService;
    @RequestMapping("/doAll")
    public Object doAll(@RequestParam Integer page, @RequestParam Integer limit , Document li, @RequestParam Map<String,Object> map) {
        map.put("page", (page-1)*limit);
        map.put("limit", limit);
        map.put("dotitle", li.getDotitle());

        return new Result("0","true",documentService.docount(map),documentService.doAll(map));
    }
    @RequestMapping("/doadd")
    public Object doadd(@RequestParam Map<String, Object> map) {
        return new Result("0","0",documentService.doadd(map),true);

    }
    @RequestMapping("/doupdate")
    public Object doupdate(@RequestParam Map<String,Object> map) {

        return new Result("0","0",documentService.doupdate(map),true);

    }
    @RequestMapping("/dodelete")
    public Object dodelete(int doid) {

        return new Result("0","0",documentService.dodelete(doid),true);

    }

}
