package com.oa.controller;

import com.oa.config.Result;
import com.oa.entity.Document;
import com.oa.entity.Documentapproval;
import com.oa.service.DocumentapprovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

//文档审核的控制层
@RestController
@RequestMapping("/Documentapproval")
public class DocumentapprovalController {
    @Autowired
    DocumentapprovalService documentapprovalService;
    @RequestMapping("/daAll")
    public Object doAll(@RequestParam Integer page, @RequestParam Integer limit , @RequestParam Map<String,Object> map) {
        map.put("page", (page-1)*limit);
        map.put("limit", limit);


        return new Result("0","true",documentapprovalService.dacount(map),documentapprovalService.daAll(map));
    }
    @RequestMapping("/daadd")
    public Object doadd(@RequestParam Map<String, Object> map) {
        return new Result("0","0",documentapprovalService.daadd(map),true);

    }
    @RequestMapping("/daupdate")
    public Object doupdate(@RequestParam Map<String,Object> map) {

        return new Result("0","0",documentapprovalService.daupdate(map),true);

    }

}
