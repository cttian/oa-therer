package com.oa.controller;

import com.oa.config.Result;
import com.oa.service.DocumentrequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

//文档申请的控制层
@RestController
@RequestMapping("/Documentrequest")
public class DocumentrequestController {
    @Autowired
    DocumentrequestService documentrequestService;
    @RequestMapping("/dradd")
    public Object doadd(@RequestParam Map<String, Object> map) {
        return new Result("0","0",documentrequestService.dradd(map),true);

    }
    @RequestMapping("/drupdate")
    public Object drupdate(@RequestParam Map<String,Object> map) {

        return new Result("0","0",documentrequestService.drupdate(map),true);

    }
}
