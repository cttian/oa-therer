package com.oa.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.oa.entity.ModuleRoles;
import com.oa.service.ModuleRolesService;
import com.oa.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//模块的控制层
@RestController
@RequestMapping("/ModulesRoles")
public class ModuleRolesController {
	@Autowired
	private ModuleRolesService moduleRolesService;
	/**
	 * 根据用户id去添加角色 根据角色id去查询 如果存在先去根据角色id删除，再去添加
	 * @return
	 */
	@RequestMapping("/addModuleByIdRoles")
	public Object addModuleByIdRoles(ModuleRoles moduleRoles,@RequestParam("ids") Object ids,int roleId) {
		if (moduleRolesService.selectRoleId(roleId)==0) {//根据角色id去查询 =0 证明没有改角色 直接去添加
			JSONArray js = JSON.parseArray(ids.toString());//将从ids.toString()中获得的字符串直接转换成对象
			Integer jg = 0;
			for (Object i : js) {
				moduleRoles.setModuleId(Integer.parseInt(i.toString()));;
				moduleRoles.setRoleId(roleId);
				jg += moduleRolesService.addModuleByIdRoles(moduleRoles);
			}
			if (jg == js.size()) {
				return new Result("0", "添加成功", jg, "后台请求成功");
			}
			Result result=new Result("0", "添加失败", 0, "后台请求成功");
			return result;
		}//>0 存在角色 先去根据角色id删除，再去添加
			moduleRolesService.deleteRoleId(roleId);//根据角色id删除
			JSONArray js = JSON.parseArray(ids.toString());//将从ids.toString()中获得的字符串直接转换成对象
			Integer jg = 0;
			for (Object i : js) {
				moduleRoles.setModuleId(Integer.parseInt(i.toString()));;
				moduleRoles.setRoleId(roleId);
				jg += moduleRolesService.addModuleByIdRoles(moduleRoles);
			}
			if (jg == js.size()) {
				return new Result("0", "添加成功", jg, "后台请求成功");
			}
			Result result=new Result("0", "添加失败", 0, "后台请求成功");
			return result;
	}
	/**
	 * 根据用户id和角色id去删除角色
	 * @return
	 */
	@RequestMapping("/removeModuleByIdRoles")
	public Object removeModuleByIdRoles(ModuleRoles moduleRoles,@RequestParam("ids") Object ids,int roleId) {
		JSONArray js = JSON.parseArray(ids.toString());//将从ids.toString()中获得的字符串直接转换成对象
		Integer jg = 0;
		for (Object i : js) {
			moduleRoles.setModuleId(Integer.parseInt(i.toString()));;
			moduleRoles.setRoleId(roleId);
			jg += moduleRolesService.removeModuleByIdRoles(moduleRoles);
		}
		if (jg == js.size()) {
			return new Result("0", "移除成功", jg, "后台请求成功");
		}
		Result result=new Result("0", "移除失败", 100, "后台请求成功");
		return result;
//		System.out.println(moduleRoles);
//		System.out.println("删除是否成功"+ moduleRolesService.removeModuleByIdRoles(moduleRoles));
//		Result result=new Result("0", "true", moduleRolesService.removeModuleByIdRoles(moduleRoles), "后台请求成功");
//		return result;
	}
}
