package com.oa.controller;

import com.oa.entity.Modules;
import com.oa.service.ModulesService;
import com.oa.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

//模块的控制层
@RestController
@RequestMapping("/Modules")
public class ModulesController {
	@Autowired
	private ModulesService modulesService;
	/**
	 * 查询所有父模块
	 * @return
	 */
	@RequestMapping("/getAllParentId")
	public Object getAllParentId() {
		return modulesService.getAllParentId();
	}

	/**
	 * 根据角色id获取所有模块
	 * 
	 * @return
	 */
	@RequestMapping("/getAllModules")
	public Object getAllModules(@RequestParam("roleId") int roleId) {
		Result result = new Result("0", "true", 10, modulesService.getAllModules(roleId));
		return result;
	}

	/**
	 * 树形组件获取所拥有的的菜单信息：，=true 勾选
	 * 
	 * @return
	 */
	@RequestMapping("/getAllModulesss")
	public List<Modules> getAllModulesss(int userId) {// 用户id
		return modulesService.getAllModulesss(userId);
	}
	/**
	 * 树形组件获取所拥有的的菜单信息：
	 * 
	 * @return
	 */
	@RequestMapping("/getAllModulessAll")
	public List<Modules> getAllModulessAll() {// 用户id
		return modulesService.getAllModulessAll();
	}

	/**
	 * 登录系统成功之后，如何根据当前用户id，获取用户所拥有的的菜单信息：
	 * 
	 * @return
	 */
	@RequestMapping("/getAllModuless")
	public List<Modules> getAllModuless(Integer userId) {// 用户id
		return modulesService.getAllModuless(userId);
	}

	/**
	 * 多条件查询角色
	 * 
	 * @param m
	 * @param page
	 * @param limit
	 * @return
	 */
	@RequestMapping("/getModules")
	public Object getModules(@RequestParam Map<String, Object> m, int page, int limit) {
		m.put("page", (page - 1) * limit);
		m.put("limit", limit);
		Result result = new Result("0", "true", modulesService.getAllModulesAllCount(m),
				modulesService.getAllModulesAll(m));
		return result;
	}

	/**
	 * 查询角色
	 * 
	 * @param m
	 * @param page
	 * @param limit
	 * @return
	 */
	@RequestMapping("/getAllModulesAlls")
	public Object getAllModulesAlls() {
		Result result = new Result("0", "true", 10, modulesService.getAllModulesAlls());
		return result;
	}

	/**
	 * 添加的方法
	 * 
	 * @return //
	 */
	@RequestMapping("/add")
	public Object add(Modules modules) {
		// 根据父级id，模块名称，查询同一模块下节点名是否存在
		if (modulesService.getModelsParentId(modules) == 0) {// 不存在
			Result result = new Result("0", "true", modulesService.add(modules), "后台请求成功");
			return result;
		}
		Result result = new Result("0", "该模块已存在", 0, "后台请求成功");
		return result;
	}

	/**
	 * 编辑角色
	 * 
	 * @return
	 */
	@RequestMapping("/update")
	public Object update(Modules modules) {
		// 根据父级id，模块名称，查询同一模块下节点名是否存在
		if (modulesService.getModelsParentId(modules) == 0) {// 不存在
			System.out.println(modules);
			System.out.println(modulesService.update(modules));
			Result result = new Result("0", "true", modulesService.update(modules), "后台请求成功");
			return result;
		}
		Result result = new Result("0", "该模块已存在", 0, "后台请求成功");
		return result;
	}

	/**
	 * 删除角色
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	public Object del(int id) {
		// 根据模块id，获取模块所拥有的角色信息;
		if (modulesService.getModelsByIdRole(id) == 0) {// 没有角色
			Result result = new Result("0", "true", modulesService.del(id), "后台请求成功");
			return result;
		}
		Result result = new Result("0", "该模块拥有角色，不能删除", 0, "后台请求成功");
		return result;
	}
}
