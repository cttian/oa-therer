package com.oa.controller;

import com.oa.entity.Note;
import com.oa.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.oa.config.Result;

import javax.annotation.Resource;
import java.util.Map;

//便签的控制层
@RestController
@RequestMapping("/note")
public class NoteController {
    @Autowired
    private NoteService noteService;

    @RequestMapping("/all")
    public Object all(@RequestParam Integer page, @RequestParam Integer limit , Note li, @RequestParam Map<String,Object> map) {
        map.put("page", (page-1)*limit);
        map.put("limit", limit);
        map.put("content", li.getContent());
        return new Result("0","true",noteService.count(map),noteService.noteAll(map));
    }
    @RequestMapping("/add")
    public Object add(@RequestParam Map<String, Object> map) {
        return new Result("0","0",noteService.add(map),true);

    }
    @RequestMapping("/update")
    public Object update(@RequestParam Map<String,Object> map) {

        return new Result("0","0",noteService.update(map),true);

    }
    @RequestMapping("/delete")
    public Object delete(int nid) {

        return new Result("0","0",noteService.delete(nid),true);

    }
}
