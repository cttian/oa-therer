package com.oa.controller;

import com.oa.dao.PublicNoticeDao;
import com.oa.entity.PublicNotice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Mr.suho
 * @Date 2021/12/7 12:19
 * @Description NoticeController
 * @Version 1.0
 */
@RequestMapping("/notice")
@RestController
public class NoticeController {
    @Autowired
    PublicNoticeDao publicNoticeDao;


    /*
     * 1.发送共信息
     * 2.往数据库存信息
     * */
    @RequestMapping("/pushnotice")
    public Integer addPublicNotice(PublicNotice publicNotice){
        System.out.println(NoticeWebScoketController.getOnlineCount());
        return publicNoticeDao.addPublicNotice(publicNotice);
    }
}
