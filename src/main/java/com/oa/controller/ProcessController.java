package com.oa.controller;


import com.oa.entity.Modules;
import com.oa.entity.Process;
import com.oa.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Mr.suho
 * @Date 2021/12/1 11:05
 * @Description ProcessController
 * @Version 1.0
 */
@RestController
@RequestMapping("process")
public class ProcessController {
    @Autowired
    ProcessService processService;

    @RequestMapping("/getAllProcess")
    public List<Map<String, Object>> getAllForms() {
        /*
        * 一级树
        * 1.获取所有值
        * 2.将需要的值循环遍历存如到数组
        * 3.返回拿到的数据
        * */
            List<Process> bg=processService.getAllProcess();//获取所有数据
            List<Map<String, Object>> bgtwo=new ArrayList<>();//新建数组存遍历的数据
            for (Process bgOne:bg){//遍历存数据到数组bober
                Map<String,Object> bgber=new HashMap<>();
                bgber.put("title",bgOne.getFormtitle());
                bgber.put("id",bgOne.getId());
                bgber.put("content",bgOne.getFormcontent());
                bgtwo.add(bgber);
            }
            return bgtwo;
        }
    /*根据id查表单*/
    @RequestMapping("/getFormById")
    public List<Process> getFormById(Integer id){
        return processService.getFormById(id);
    }
    /*动态表单添加*/
    @RequestMapping("/insertproces")
    public Integer insertProcess(Process process) {
        return processService.insertProcess(process);
    }
}
