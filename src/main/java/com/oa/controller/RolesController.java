package com.oa.controller;

import com.oa.entity.Roles;
import com.oa.service.ModulesService;
import com.oa.service.RolesService;
import com.oa.service.UsersService;
import com.oa.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

//角色控制层
@RestController
@RequestMapping("/roles")
public class RolesController {
    @Autowired
    private RolesService rolesService;
    @Autowired
    private UsersService usersService;// 自动注入服务层
    @Autowired
    private ModulesService modulesService;

    /**
     * 根据用户id获取所有角色
     *
     * @return
     */
    @RequestMapping("/getAllRoles")
    public Object getAllRoles(@RequestParam("userId") int userId) {
        Result result = new Result("0", "true", 10, rolesService.getAllRoles(userId));
        return result;
    }

    /**
     * 多条件查询角色
     *
     * @param m
     * @param page
     * @param limit
     * @return
     */
    @PostMapping("/getRoles")
    public Object getRoles(@RequestParam Map<String, Object> m, int page, int limit) {
        m.put("page", (page - 1) * limit);
        m.put("limit", limit);
        Result result = new Result("0", "true", rolesService.getAllRolesAllCount(m), rolesService.getAllRolesAll(m));
        return result;
    }

    /**
     * 查询角色
     *
     * @param m
     * @param page
     * @param limit
     * @return
     */
    @PostMapping("/getAllRolesAlls")
    public Object getAllRolesAlls(@RequestParam Map<String, Object> m) {
        Result result = new Result("0", "true", 10, rolesService.getAllRolesAlls(m));
        return result;
    }

    /**
     * 添加的方法
     *
     * @return
     */
    @RequestMapping("/add")
    public Object add(Roles roles, String name) {
        //根据用户id，获取用户所拥有的角色信息usersService.getUserByIdRole(id);
        //存在的角色不能在添加 rolesService.getAllRolesName(name)  根据角色名去查询当角色是否存在
        if (rolesService.getAllRolesName(name) == 0) {//==null没有这个角色
            Result result = new Result("0", "true", rolesService.add(roles), "后台请求成功");
            return result;
        }
        Result result = new Result("0", "该角色已存在", 0, "后台请求成功");
        return result;
    }

    /**
     * 编辑角色
     *
     * @return
     */
    @RequestMapping("/update")
    public Object update(Roles roles, String name) {
        //存在的角色不能在添加 rolesService.getAllRolesName(name)  根据角色id去查询当角色是否存在
        if (rolesService.getAllRolesName(name) == 0) {//==null没有这个角色
            Result result = new Result("0", "true", rolesService.update(roles), "后台请求成功");
            return result;
        }
        Result result = new Result("0", "该角色已存在", 0, "后台请求成功");
        return result;
    }

    /**
     * 删除角色
     *
     * @return
     */
    @RequestMapping("/del")
    public Object del(int id) {
        //当前登录的用户不能删除 usersService.getByUserName(loginName)  根据角色id去查询权限
        if (modulesService.getRoleByIdModels(id) == 0) {//==0 没有分配角色
            Result result = new Result("0", "true", rolesService.del(id), "后台请求成功");
            return result;
        }
        Result result = new Result("0", "不能删除，该角色拥有权限", 0, "后台请求成功");
        return result;
    }
}
