package com.oa.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.oa.entity.Section;


import com.oa.entity.Users;
import com.oa.service.SectionService;
import com.oa.util.Result;

import com.oa.vo.UserVO02;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author Mr.suho
 * @Date 2021/11/23 14:39
 * @Description SectionController
 * @Version 1.0
 */
@RestController
@RequestMapping("section")
public class SectionControllers {
    @Autowired
    SectionService sectionService;

    /*@Autowired
    SectionMapper sectionMapper;*/
    /*根据部门名称模糊查询所有部门-*/
    @RequestMapping("/getAllSection")
    public Result getAllSection(String sname) {
        Result result = new Result("0", "true", 10, sectionService.getAllSection(sname));
        return result;
    }
    /*根据部门id查询部门详情*/
    @RequestMapping("/getAllSectionBySid")
    public Result getAllSectionBySid(Integer ids){
        List<UserVO02> userVO02s = sectionService.getAllSectionBySid(ids);
        Result result = new Result("","",10,userVO02s);
        System.out.println(userVO02s);
        return  result;
    }
    /*根据用户id查所属部门*/
    @RequestMapping("/getAllSectionByUserId")
    public List<Section> getAllSectionByUserId(Integer userId) {
        List<Section> section = sectionService.getAllSectionByUserId(userId);
        return section;
    }

    /*无用--查询所有部门及用户表所属部门人数*/
    @RequestMapping("/getAllSections")
    public Result getAllSections() {
        Result result = new Result("", "", sectionService.getSectionCount(), sectionService.getAllSections());
        return result;
    }




    /*添加*/
    @RequestMapping("/addSection")
    //根据部门名查询是否有这个部门，如果有此部门就不能再添加
    public Object addSection(Section section, String sname) {
        if (sectionService.getAllSectionByName(sname) == 0) {//==null没有这个角色
            Result result = new Result("0", "true", sectionService.addSection(section), "后台请求成功");
            return result;
        }
        Result result = new Result("0", "该角色已存在", 0, "后台请求成功");
        return result;
    }


    /*修改*/
    @RequestMapping("/updateSection")
    public Integer updateSection(Section section) {
        return sectionService.updateSection(section);
    }

    /*删除*/
    @RequestMapping("/delSection")
    public Object delSection(Integer id) {
        if (sectionService.selectSectionBySid(id) == 0){
            Result result = new Result("","",10,sectionService.selectSectionBySid(id));
            System.out.println(result);
            return result;
        }
        Result result = new Result("0", "该部门下有员工，不可删除", 0, "后台请求成功");
        return result;

    }

    /*批量删除*/
    @RequestMapping("/delAllSection")
    public Object delAllSection(@RequestParam("ids") Object ids) {
        JSONArray js = JSON.parseArray(ids.toString());//将从ids.toString()中获得的字符串直接转换成对象
        Integer jg = 0;
        for (Object i : js) {

            jg += sectionService.delAllSection(Integer.parseInt(i.toString()));
        }

        if (jg == js.size()) {
            return new Result("0", "删除成功", jg, "后台请求成功");
        }
        Result result = new Result("0", "删除失败", 0, "后台请求成功");
        return result;
    }
}
