package com.oa.controller;

import com.oa.config.Result;
import com.oa.entity.Addressbook;
import com.oa.entity.Task;
import com.oa.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/task")
public class TaskController {
    @Autowired
    TaskService taskService;

    @RequestMapping("/taAll")
    public Object aAll(@RequestParam Integer page, @RequestParam Integer limit , Task li, @RequestParam Map<String,Object> map) {
        map.put("page", (page-1)*limit);
        map.put("limit", limit);
        map.put("tname", li.getTname());
        return new Result("0","true",taskService.tacount(map),taskService.taAll(map));
    }
    @RequestMapping("/taadd")
    public Object add(@RequestParam Map<String, Object> map) {
        return new Result("0","0",taskService.taadd(map),true);

    }
    @RequestMapping("/taupdate")
    public Object update(@RequestParam Map<String,Object> map) {

        return new Result("0","0",taskService.taupdate(map),true);

    }
    @RequestMapping("/tadelete")
    public Object delete(int tid) {

        return new Result("0","0",taskService.tadelete(tid),true);

    }
}
