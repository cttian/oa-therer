package com.oa.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.oa.entity.Section;
import com.oa.entity.Users;
import com.oa.service.UsersService;
import com.oa.util.MD5Util;
import com.oa.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

//用户的控制层
@RestController
@RequestMapping("/users")
public class UsersController {
	@Autowired
	private UsersService usersService;// 自动注入服务层


	/**
	 * 多条件查询所有用户
	 * 
	 * @param m
	 * @param page
	 * @param limit
	 * @return
	 */
	@RequestMapping("/getUser")
	public Object getUser(@RequestParam Map<String, Object> m, int page, int limit) {
		m.put("page", (page - 1) * limit);
		m.put("limit", limit);
		Result result = new Result("0", "true", usersService.getUserCount(m), usersService.getUser(m));
		return result;
	}
	/*查询所有部门名称*/
	@RequestMapping("/getSectionName")
	public List<Section> getSectionName(){
		return usersService.getSectionName();
	}


	/**
	 * 多条件查询用户和角色
	 * 
	 * @param m
	 * @param page
	 * @param limit
	 * @return
	 */
	@RequestMapping("/getUserByIdRoles")
	public Object getUserById(@RequestParam Map<String, Object> m, int page, int limit) {
		m.put("page", (page - 1) * limit);
		m.put("limit", limit);
		Result result = new Result("0", "true", usersService.getUserByIdCounnt(m), usersService.getUserById(m));
		return result;
	}
	/**
	 * 添加是查看用户是否存在
	 */
	@RequestMapping("/selectUser")
	public Object selectUser(String loginName) {
		if (usersService.getByUserName(loginName) == null) {// ==null无值
			Result result = new Result("0", "true", 0, "success");
			return result;
		}
		Result result = new Result("0", "用户名已存在", 2, "false");
		return result;
	}
	/**
	 * 添加的方法 用户名不能重复
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	public Object add(Users users, String loginName) {

		if (usersService.getByUserName(loginName) == null) {// ==null无值
			String salt = MD5Util.getSalt();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
			users.setPassword(MD5Util.generate(users.getPassword(), salt));
			users.setIsLockout(0);//0为锁定
			users.setPsdWrongTime(0);
			users.setCrateTime(df.format(new Date()));
			users.setSalts(salt);
			int i = usersService.add(users);
			Result result = new Result("0", "true", i, "后台请求成功");
			return result;
		}
		Result result = new Result("0", "用户名已存在", 0, "success");
		return result;
	}

	/**
	 * 编辑用户 修改用户是否锁定 和被锁定的时间
	 * 
	 * @return
	 */
	@RequestMapping("/updateIsLockout")
	public Object updateIsLockout(Users users) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		users.setLockTime(df.format(new Date()));
		Result result = new Result("0", "true", usersService.updateIsLockout(users), "后台请求成功");
		return result;
	}

	/**
	 * 重置用户密码
	 * 
	 * @return
	 */
	@RequestMapping("/updatePwd")
	public Object updatePwd(Users users) {
		String salt=MD5Util.getSalt();//获取加密盐
		users.setSalts(salt);
		users.setPassword(MD5Util.generate("111111", salt));
		Result result = new Result("0", "true", usersService.updatePwd(users), "后台请求成功");
		return result;
	}

	/**
	 * 编辑用户
	 * 
	 * @return
	 */
	@RequestMapping("/update")
	public Object update(Users users) {
		Result result = new Result("0", "true", usersService.update(users), "后台请求成功");
		return result;
	}


	/**
	 * 删除用户
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	public Object del(int id, String loginName) {
		// 根据用户id，获取用户所拥有的角色信息usersService.getUserByIdRole(id);
		// 当前登录的用户不能删除 usersService.getByUserName(loginName) 根据名字去查询当前用户
		if (usersService.getByUserName(loginName) == null || usersService.getUserByIdRole(id) == 0) {// ==null不是当前用户
			Result result = new Result("0", "true", usersService.del(id), "后台请求成功");
			return result;
		}
		Result result = new Result("0", "不能删除，该用户分配有角色或是当前登录用户", 0, "后台请求成功");
		return result;
	}
	/**
	 * 批量删除用户
	 *
	 * @return
	 */
	@RequestMapping("/delAll")
	public Object delAll(@RequestParam("ids") Object ids, String loginName) {
		// 根据用户id，获取用户所拥有的角色信息usersService.getUserByIdRole(id);
		// 当前登录的用户不能删除 usersService.getByUserName(loginName) 根据名字去查询当前用户
		JSONArray js = JSON.parseArray(ids.toString());//将从ids.toString()中获得的字符串直接转换成对象
		Integer jg = 0;
		for (Object i : js) {
			jg += usersService.getUserByIdRole(Integer.parseInt(i.toString()));
			System.out.println(jg);
		}
		if (jg == 0) {//jg=0没有分配角色
			Integer jgs=0;
			System.out.println("循环查询是否分配角色个数"+jg);
			System.out.println(usersService.getByUserName(loginName));
//			if (usersService.getByUserName(loginName) == null) {// ==null不是当前用户
				for (Object i : js) {//循环去删除
					jgs += usersService.del(Integer.parseInt(i.toString()));
					System.out.println("删除个数"+jgs);
				}
				if(jgs == js.size()){//判断是否全部删除
					Result result = new Result("0", "true", 2, "后台请求成功");
					return result;
				}
//			}
		}
		Result result = new Result("0", "不能删除，你选中的用户分配有角色", 0, "后台请求成功");
		return result;
	}

}
