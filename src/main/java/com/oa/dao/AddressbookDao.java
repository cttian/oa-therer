package com.oa.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
@Mapper
public interface AddressbookDao {
    //外部通讯
    List<Map<String,Object>> aAll (Map<String,Object> map);

    Integer count(Map<String,Object> map);

    Integer add(Map<String,Object> map);

    Integer update(Map<String,Object> map);

    Integer delete(int id);

}
