package com.oa.dao;

import com.oa.entity.BecomeForm;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author Mr.suho
 * @Date 2021/12/6 9:09
 * @Description ApplyDao
 * @Version 1.0
 */
@Mapper
public interface ApplyDao {
    List<BecomeForm> getMyapplyByUserId(Integer id);
    //将用户id添加进数据库
    Integer addMyapplyUserId(Integer userId);
}
