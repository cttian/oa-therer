package com.oa.dao;

import com.oa.entity.BecomeForm;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author Mr.suho
 * @Date 2021/12/5 16:59
 * @Description BecomDao
 * @Version 1.0
 */
@Mapper
public interface BecomeDao {
    /*存员工填写的表单数据*/
    Integer addBecomeForm(BecomeForm becomeForm);
}
