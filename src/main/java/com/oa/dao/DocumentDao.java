package com.oa.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface DocumentDao {
    List<Map<String,Object>> doAll (Map<String,Object> map);

    Integer docount(Map<String,Object> map);

    Integer doadd(Map<String,Object> map);

    Integer doupdate(Map<String,Object> map);

    Integer dodelete(int doid);
}
