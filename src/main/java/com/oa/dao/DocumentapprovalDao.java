package com.oa.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
@Mapper
public interface DocumentapprovalDao {
    List<Map<String,Object>> daAll (Map<String,Object> map);

    Integer dacount(Map<String,Object> map);

    Integer daadd(Map<String,Object> map);

    Integer daupdate(Map<String,Object> map);


}
