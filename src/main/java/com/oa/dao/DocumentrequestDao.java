package com.oa.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;
@Mapper
public interface DocumentrequestDao {
    Integer dradd(Map<String,Object> map);
    Integer drupdate(Map<String,Object> map);
}
