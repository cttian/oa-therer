package com.oa.dao;

import com.oa.entity.ModuleRoles;
import org.apache.ibatis.annotations.Mapper;

//模块和用户dao层
@Mapper
public interface ModuleRolesDao {
	/**
	 * 根据角色id去添加模块
	 * @param userId
	 * @return
	 */
	public Integer addModuleByIdRoles(ModuleRoles moduleRoles);
	
	/**
	 * 根据模块和角色id去删除模块
	 * @param userId
	 * @return
	 */
	public Integer removeModuleByIdRoles(ModuleRoles moduleRoles);
	/**
	 * 如果存在先去根据角色id删除
	 * @param moduleRoles
	 * @return
	 */
	public Integer deleteRoleId(Integer roleId);
	/**
	 * 根据角色id去查询
	 * @param roleId
	 * @return
	 */
	public Integer selectRoleId(Integer roleId);
}
