package com.oa.dao;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
@Mapper
public interface NoteDao {
    List<Map<String,Object>> noteAll (Map<String,Object> map);

    Integer count(Map<String,Object> map);

    Integer add(Map<String,Object> map);

    Integer update(Map<String,Object> map);

    Integer delete(int nid);
}
