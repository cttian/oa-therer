package com.oa.dao;


import com.oa.entity.Process;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author Mr.suho
 * @Date 2021/12/1 11:08
 * @Description ProcessDao
 * @Version 1.0
 */
@Mapper
public interface ProcessDao {
    /*查询所有*/
    List<Process> getAllProcess();
    /*根据id查表单数据*/
    List<Process> getFormById(Integer id);
    /*添加*/
    Integer insertProcess(Process process);

    void createAutoTask(String tableName, Map<String, String> tableFields);
}
