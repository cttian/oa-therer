package com.oa.dao;

import com.oa.entity.PublicNotice;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author Mr.suho
 * @Date 2021/12/7 12:00
 * @Description PublicNoticeDao
 * @Version 1.0
 */
@Mapper
public interface PublicNoticeDao {
    /*保存公共信息*/
    Integer addPublicNotice(PublicNotice publicNotice);
}
