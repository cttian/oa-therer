package com.oa.dao;
//角色的dao层接口

import com.oa.entity.Roles;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface RolesDao {
	/**
	 * 根据角色名去查询角色
	 * @param name
	 * @return
	 */
	public Integer getAllRolesName(String name);
	/**
	 * 根据用户查询所有角色
	 * @return
	 */
	public List<Map<String, Object>> getAllRoles(int userId);
	/**
	 * 查询所有角色
	 * @param m
	 * @return
	 */
	public List<Map<String, Object>> getAllRolesAll(Map<String, Object> m);
	public List<Map<String, Object>> getAllRolesAlls(Map<String, Object> m);
	public Integer getAllRolesAllCount(Map<String, Object> m);
	/**
	 * 添加角色
	 * @param roles
	 * @return
	 */
	public Integer add(Roles roles);
	/**
	 * 修改角色
	 * @param roles
	 * @return
	 */
	public Integer update(Roles roles);
	/**
	 * 删除角色
	 * @param id
	 * @return
	 */
	public Integer del(int id);
	

}
