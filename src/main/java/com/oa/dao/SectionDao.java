package com.oa.dao;

import com.oa.entity.Section;
import com.oa.entity.Section;
import com.oa.entity.Users;
import com.oa.vo.UserVO02;
import com.oa.vo.UserVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author Mr.suho
 * @Date 2021/11/23 14:32
 * @Description SectionDao
 * @Version 1.0
 */
@Mapper
public interface SectionDao {
    /*分页条件查询所有*/
    List<Section> getAllSection(String sname);
    /*根据用户id查询所在部门*/
    List<Section> getAllSectionByUserId(Integer userId);
    /*根据部门id查询部门详情*/
    List<UserVO02> getAllSectionBySid(Integer ids);
    /*查询部门人数-部门表&用户表*/
    List<Map<String, Object>> getAllSections();
    /*根据部门名查部门人数*/
    Integer getAllSectionByName(String sname);

    /*查询总数量*/
    Integer getSectionCount();
    /*添加*/
    Integer addSection(Section section);
    /*编辑*/
    Integer updateSection(Section section);
    /*删除*/
    Integer delSection(Integer id);
    /*根据部门id查询部门下人数*/
    Integer selectSectionBySid(Integer id);

    /*批量删除*/
    Integer delAllSection(Integer id);
    List<UserVo> userlist();
}
