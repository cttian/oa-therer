package com.oa.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

//我的任务接口
@Mapper
public interface TaskDao {
    List<Map<String,Object>> taAll (Map<String,Object> map);

    Integer tacount(Map<String,Object> map);

    Integer taadd(Map<String,Object> map);

    Integer taupdate(Map<String,Object> map);

    Integer tadelete(int tid);

}
