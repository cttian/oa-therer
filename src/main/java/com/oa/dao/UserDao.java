package com.oa.dao;

import com.oa.entity.Section;
import com.oa.entity.Users;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

//用户dao层接口
@Mapper
public interface UserDao {

	/**
	 * 查询所有用户
	 * @param m
	 * @return
	 */
	public List<Map<String, Object>> getZiXun(Map<String, Object> m);
	public Integer getZiXunCount(Map<String, Object> m);
	/**
	 * 根据用户id，获取用户所拥有的角色信息
	 * @param id
	 * @return
	 */
	public Integer getUserByIdRole(int id);
	public Users getByUserId(Integer userId);
	/**
	 * 登录的方法，根据用户名去去查询对比
	 * 
	 * @param userName
	 * @return
	 */
	public Users getByUserName(String userName);
	/**
	 * 修改密码错误次数
	 * @param psdWrongTime
	 * @return
	 */
	public Integer getBypsdWrongTime(int id);
	/**
	 * 将密码错误次数清零
	 * @param users
	 * @return
	 */
	public Integer getBypsdWrongTimeClear(int id);

	/*查询所有部门名称*/
	List<Section>getSectionName();
	
	/**
	 * 根获取所有用户信息多条件查询
	 * @param m
	 * @return
	 */
	public List<Map<String, Object>> getUser(Map<String, Object> m);
	public Integer getUserCount(Map<String, Object> m);
	/**
	 * 根据当前用户id，获取用户所拥有的的菜单信息 获取所有用户信息多条件查询
	 * @param m
	 * @return
	 */
	public List<Map<String, Object>> getUserById(Map<String, Object> m);
	public Integer getUserByIdCounnt(Map<String, Object> m);
	/**
	 * 用户的添加
	 * @param users
	 * @return
	 */
	public Integer add(Users users);
	/**
	 * 编辑用户  修改用户密码
	 * @param users
	 * @return
	 */
	public Integer updatePwd(Users users);
	/**
	 * 编辑用户  修改用户是否锁定 和被锁定的时间 禁用
	 * @param users
	 * @return
	 */
	public Integer updateIsLockout(Users users);
	/**
	 * 编辑用户 修改用户最后一次登录时间
	 * @param users
	 * @return
	 */
	public Integer updateLastLoginTime(Users users);
	/**
	 * 编辑用户 修改用户
	 * @param users
	 * @return
	 */
	public Integer update(Users users);

	/**
	 * 删除用户
	 * @param id
	 * @return
	 */
	public Integer del(int id);

}
