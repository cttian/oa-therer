package com.oa.dao;

import com.oa.entity.UserRoles;
import org.apache.ibatis.annotations.Mapper;

//用户表和角色表的dao层接口
@Mapper
public interface UserRolesDao {
	/**
	 * 根据用户id去添加角色
	 * @param userId
	 * @return
	 */
	public Integer addUsersByIdRoles(UserRoles userRoles);
	/**
	 * 根据用户和角色id去删除角色 
	 * @param userId
	 * @return
	 */
	public Integer removeUsersByIdRoles(UserRoles userRoles);
}
