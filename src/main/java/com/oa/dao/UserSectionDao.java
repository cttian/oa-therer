package com.oa.dao;


import com.oa.entity.UserSection;
import org.apache.ibatis.annotations.Mapper;

//用户表和部门表的dao层接口
@Mapper
public interface UserSectionDao {
	/**
	 * 根据用户id去添加部门
	 * @param userId
	 * @return
	 */
	public Integer addUsersByIdSection(UserSection userSection);
	/**
	 * 根据用户和角色id去删除部门
	 * @param userId
	 * @return
	 */
	public Integer removeUsersByIdSection(UserSection userSection);
}
