package com.oa.entity;

import lombok.Data;
//外部通讯录表
@Data
public class Addressbook {
    private  Integer id;
    private String abname;
    private String absex;
    private String abtel;
    private String  msgid;
    private String  one;
    private String  two;
}
