package com.oa.entity;

import lombok.Data;

//签到表的实体类
@Data
public class Asker {
	private Integer askerId;// id
	private String askerName;// 签到人名称
	private String checkState;// 签到状态
	private String checkInTime;// 签到时间
	private String checkOutTime;// 签退时间
	private Integer weight;// 权重
	private String bakConten;// 内容
	private Integer exOne;// 备用
	private String exTwo;// 备用
}
