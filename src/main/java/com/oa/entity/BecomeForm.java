package com.oa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author Mr.suho
 * @Date 2021/12/5 16:54
 * @Description BecomeForm
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BecomeForm {
    private Integer id;
    private String sname;
    private String ssex;
    private String sxl;
    private String sjg;
    private String ssection;
    private String datesrdate;
    private String datesoverdate;
    private String sisover;
    private String sydates;
    private String seditor;
    private String sshyj;
    private Date createtime;
    private Integer userid;
    private Integer state;
}
