package com.oa.entity;

import lombok.Data;
//文档表
@Data
public class Document {
    private  Integer doid;
    private String dotitle;//标题
    private String dodescription;//标题描述
    private String docontent;//内容
    private String dolx;//归档类型
    private Integer docjr;//创建人id
    private String dotime;//创建时间
    private String dosf;//是否归档
    private String dostart;//状态id
    private String dododo;//预留字段


}
