package com.oa.entity;

import lombok.Data;
//文档审批
@Data
public class Documentapproval {
    private int daid;
    private  int doid;//申请表id
    private  String daapplicant;//审核人
    private  String daThetime;//审核时间
    private  String daremark;//审核备注
   private  String dostart;//审核状态
    private String dayl;//预留

}
