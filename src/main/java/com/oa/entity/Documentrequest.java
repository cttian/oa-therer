package com.oa.entity;

import lombok.Data;
//文档申请表
@Data
public class Documentrequest {
    private  Integer doid;
    private  String applicant;//申请人
    private  String Thetime;//申请时间
    private  String remark;//备注
    private  String dostart;//状态
    private String dryl;//预留
}
