package com.oa.entity;

import lombok.Data;

//用户模块关联表
@Data
public class ModuleRoles {
	private Integer id;//id
	private Integer roleId;//角色id
	private Integer moduleId;//模块id
}
