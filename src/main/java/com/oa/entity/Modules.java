package com.oa.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

//模块表
@Data
public class Modules {
	private Integer id;//id
	private String name;// 模块编号
	private String title;// 模块编号
	private Boolean checked;// 模块编号 true
	private Integer parentId;//父模块编号
	private String path;//模块路径
	private String weight;//权重
	private String one;// 预留
	private String two;//预留
	private List<Modules> children = new ArrayList<>();
}
