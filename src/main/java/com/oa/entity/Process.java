package com.oa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Mr.suho
 * @Date 2021/12/1 11:06
 * @Description Process
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Process {
    private Integer id;
    private String formtitle;
    private String formcontent;
    private Integer parentid;
}
