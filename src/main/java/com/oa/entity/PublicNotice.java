package com.oa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * @Author Mr.suho
 * @Date 2021/12/7 11:31
 * @Description 公共信息-通知
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublicNotice {
    private Integer pnId;
    private String pnTitle;
    private String pnContent;
}
