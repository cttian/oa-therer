package com.oa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @Author Mr.suho
 * @Date 2021/11/23 14:32
 * @Description Section
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Section {
    private Integer id;
    private String sname;
    private String sintroduce;
    private String idx;
    private String parentid;
}
