package com.oa.entity;

import lombok.Data;

//我的任务
@Data
public class Task {
    private  int  tid;
    private String tname;//任务标题
    private String tcum;//任务标题描述
    private String tks;//任务开始时间
    private String tjs;//任务结束时间
    private String tzt;//任务状态
}
