package com.oa.entity;

import lombok.Data;

//用户和角色的关联表实体类
@Data
public class UserRoles {
	private Integer id;//id
	private Integer userId;//用户id
	private Integer roleId;//角色id
}
