package com.oa.entity;

import lombok.Data;

//用户和部门的关联表实体类
@Data
public class UserSection {
	private Integer id;//id
	private Integer userid;//用户id
	private Integer sid;//角色id
}
