package com.oa.entity;

import lombok.Data;

//用户实体类
@Data
public class Users {
	private Integer id;//-- id
	private String loginName;// 登录名
	private String password;// 密码
	private Integer isLockout;//是否锁定
	private String lastLoginTime;//最后一次登录时间
	private String crateTime;//账户创立时间
	private Integer psdWrongTime;//密码错误次数
	private String lockTime;//被锁定的时间
	private String protectEmail;//密保邮箱
	private String protectMtel;//密保手机号
	private Integer sid;//部门id
	private Integer msgid;//留言id
	private String salts;//加盐

}
