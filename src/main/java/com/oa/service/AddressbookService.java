package com.oa.service;

import java.util.List;
import java.util.Map;
//通讯录服务层接口
public interface AddressbookService {
    //外部通讯
    List<Map<String,Object>> aAll (Map<String,Object> map);

    Integer count(Map<String,Object> map);

    Integer add(Map<String,Object> map);

    Integer update(Map<String,Object> map);

    Integer delete(int id);

}
