package com.oa.service;

import com.oa.entity.BecomeForm;

import java.util.List;

/**
 * @Author Mr.suho
 * @Date 2021/12/6 9:10
 * @Description ApplyService
 * @Version 1.0
 */
public interface ApplyService {
    List<BecomeForm> getMyapplyByUserId(Integer id);
    Integer addMyapplyUserId(Integer userId);
}
