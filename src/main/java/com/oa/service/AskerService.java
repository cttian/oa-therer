package com.oa.service;

import com.oa.entity.Asker;

import java.util.List;
import java.util.Map;

//签到服务层接口
public interface AskerService {
	/**
	 * 多条件查询签到表 多条件（用户名、签到时间、签到状态）
	 * 
	 * @param m
	 * @return
	 */
	public List<Map<String, Object>> getAllAsker(Map<String, Object> m);

	public Integer getAllAskerCount(Map<String, Object> m);

	/**
	 * 签到
	 * 
	 * @param asker
	 * @return
	 */
	public Integer add(Asker asker);
	/**
	 * 根据登录名查询是否该签到
	 * @param loginName
	 * @return
	 */
	public Integer askerYesNo(String loginName);
	/**
	 * 根据登录名查询今天是否签到，已签到不在显示签到按钮
	 * @param loginName
	 * @return
	 */
	public Integer askerYesNoAsker(Asker asker);


	/**
	 * 签退
	 * 
	 * @param asker
	 * @return
	 */
	public Integer update(Asker asker);
//	/**
//	 * 修改签到后咨询师的权重
//	 * @param asker
//	 * @return
//	 */
//	public Integer updateWeight(Asker asker);
	/**
	 * 根据登录人查询今天签到时间
	 * @param asker
	 * @return
	 */
	public String askerTime(Asker asker);
	/**
	 * 根据登录人查询今天是否签退
	 * @param asker
	 * @return
	 */
	public Integer askerOutTime(Asker asker);
}
