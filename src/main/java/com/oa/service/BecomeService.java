package com.oa.service;

import com.oa.entity.BecomeForm;

/**
 * @Author Mr.suho
 * @Date 2021/12/5 17:00
 * @Description BecomService
 * @Version 1.0
 */
public interface BecomeService {
    Integer addBecomeForm(BecomeForm becomeForm);
}
