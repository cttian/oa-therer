package com.oa.service;

import java.util.List;
import java.util.Map;
//文档服务层接口
public interface DocumentService {
    List<Map<String,Object>> doAll (Map<String,Object> map);

    Integer docount(Map<String,Object> map);

    Integer doadd(Map<String,Object> map);

    Integer doupdate(Map<String,Object> map);

    Integer dodelete(int doid);
}
