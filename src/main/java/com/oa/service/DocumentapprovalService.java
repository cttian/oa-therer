package com.oa.service;

import java.util.List;
import java.util.Map;

public interface DocumentapprovalService {
    List<Map<String,Object>> daAll (Map<String,Object> map);

    Integer dacount(Map<String,Object> map);

    Integer daadd(Map<String,Object> map);

    Integer daupdate(Map<String,Object> map);

}
