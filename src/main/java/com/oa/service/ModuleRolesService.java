package com.oa.service;

import com.oa.entity.ModuleRoles;

//模块的服务层接口
public interface ModuleRolesService {
	/**
	 * 根据角色id去添加模块
	 * @param userId
	 * @return
	 */
	public Integer addModuleByIdRoles(ModuleRoles moduleRoles);
	/**
	 * 根据模块和角色id去删除模块
	 * @param userId
	 * @return
	 */
	public Integer removeModuleByIdRoles(ModuleRoles moduleRoles);
	/**
	 * 如果存在先去根据角色id删除
	 * @param moduleRoles
	 * @return
	 */
	public Integer deleteRoleId(Integer roleId);
	/**
	 * 根据角色id去查询
	 * @param roleId
	 * @return
	 */
	public Integer selectRoleId(Integer roleId);
}
