package com.oa.service;

import com.oa.entity.Modules;

import java.util.List;
import java.util.Map;

//模块的服务层接口
public interface ModulesService {
	/**
	 * 查询所有模块 返回Modules
	 * @return
	 */
	public Modules getAll();
	/**
	 * 查询所有父模块
	 * @param modules
	 * @return
	 */
	public List<Modules> getAllParentId();
	/**
	 * 根据模块id，查询同一模块下节点名是否存在
	 * @param id
	 * @return
	 */
	public Integer getModelsParentId(Modules modules);
	/**
	 * 根据模块id，获取模块所拥有的角色信息
	 * @param id
	 * @return
	 */
	public Integer getModelsByIdRole(int id);
	/**
	 * 根据角色id去查询权限
	 * @param id
	 * @return
	 */
	public Integer getRoleByIdModels(int id);
	/**
	 * 根据用户查询所有模块
	 * @return
	 */
	public List<Map<String, Object>> getAllModules(int roleId);
	/**
	 * 获取所拥有的的菜单信息：
	 * @param userId
	 * @return
	 */
	public List<Modules> getAllModulessAll();
	/**
	 * 登录系统成功之后，如何根据当前用户id，获取用户所拥有的的菜单信息：
	 * @param userId
	 * @return
	 */
	public List<Modules> getAllModuless(int userId);
	/**
	 * 查询所有模块
	 * @param m
	 * @return
	 */
	public List<Map<String, Object>> getAllModulesAll(Map<String, Object> m);
	public List<Modules> getAllModulesAlls();
	public Integer getAllModulesAllCount(Map<String, Object> m);
	/**
	 * 添加模块
	 * @param roles
	 * @return
	 */
	public Integer add(Modules modules);
	/**
	 * 修改模块
	 * @param roles
	 * @return
	 */
	public Integer update(Modules modules);
	/**
	 * 删除模块
	 * @param id
	 * @return
	 */
	public Integer del(int id);
	/**
	 * 查询所有的方法
	 * @return
	 */
	public List<Modules> getAllModulesss(int userId);
}
