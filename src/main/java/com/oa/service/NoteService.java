package com.oa.service;

import java.util.List;
import java.util.Map;
//便签服务层接口
public interface NoteService {
    List<Map<String,Object>> noteAll (Map<String,Object> map);

    Integer count(Map<String,Object> map);

    Integer add(Map<String,Object> map);

    Integer update(Map<String,Object> map);

    Integer delete(int nid);
}
