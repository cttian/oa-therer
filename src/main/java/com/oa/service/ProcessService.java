package com.oa.service;


import com.oa.entity.Process;

import java.util.List;
import java.util.Map;

/**
 * @Author Mr.suho
 * @Date 2021/12/1 11:09
 * @Description ProcessService
 * @Version 1.0
 */
public interface ProcessService {
    List<Process> getAllProcess();
    Integer insertProcess(Process process);
    List<Process> getFormById(Integer id);
}
