package com.oa.service;

import com.oa.entity.PublicNotice;

/**
 * @Author Mr.suho
 * @Date 2021/12/7 12:00
 * @Description PublicNoticeService
 * @Version 1.0
 */
public interface PublicNoticeService {
    /*保存公共信息*/
    Integer addPublicNotice(PublicNotice publicNotice);
}
