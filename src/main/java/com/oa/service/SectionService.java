package com.oa.service;

import com.oa.entity.Section;
import com.oa.entity.Users;
import com.oa.vo.UserVO02;
import com.oa.vo.UserVo;

import java.util.List;
import java.util.Map;

/**
 * @Author Mr.suho
 * @Date 2021/11/23 14:34
 * @Description SectionService
 * @Version 1.0
 */
public interface SectionService {
    List<Section> getAllSection(String sname);
    /*查询部门人数-部门表&用户表*/
    List<Map<String, Object>> getAllSections();
    /*根据用户id查询所在部门*/
    List<Section> getAllSectionByUserId(Integer userId);
    /*根据部门id查询部门详情*/
    List<UserVO02> getAllSectionBySid(Integer ids);
    /*根据部门名查询部门的数量*/
    public Integer getAllSectionByName(String sname);

    Integer getSectionCount();
    Integer addSection(Section section);
    Integer updateSection(Section section);
    Integer delSection(Integer id);
    /*根据部门id查询部门下人数*/
    Integer selectSectionBySid(Integer id);

    /*批量删除*/
    Integer delAllSection(Integer id);
    List<UserVo> userlist();


}
