package com.oa.service;

import java.util.List;
import java.util.Map;

public interface TaskService {
    List<Map<String,Object>> taAll (Map<String,Object> map);

    Integer tacount(Map<String,Object> map);

    Integer taadd(Map<String,Object> map);

    Integer taupdate(Map<String,Object> map);

    Integer tadelete(int tid);
}
