package com.oa.service;

import com.oa.entity.UserRoles;

//用户和角色的服务层接口
public interface UserRolesService {
	/**
	 * 根据用户id去添加角色
	 * @param userId
	 * @return
	 */
	public Integer addUsersByIdRoles(UserRoles userRoles);
	/**
	 * 根据用户和角色id去删除角色 
	 * @param userId
	 * @return
	 */
	public Integer removeUsersByIdRoles(UserRoles userRoles);
}
