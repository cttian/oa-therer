package com.oa.service;


import com.oa.entity.UserSection;

//用户和角色的服务层接口
public interface UserSectionService {
	/**
	 * 根据用户id去添加部门
	 * @param userid
	 * @return
	 */
	public Integer addUsersByIdSection(UserSection userSection);
	/**
	 * 根据用户和角色id去删除部门
	 * @param userid
	 * @return
	 */
	public Integer removeUsersByIdSection(UserSection userSection);
}
