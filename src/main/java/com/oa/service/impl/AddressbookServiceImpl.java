package com.oa.service.impl;

import com.oa.dao.AddressbookDao;
import com.oa.service.AddressbookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
//通讯录接口实现类
@Service
public class AddressbookServiceImpl implements AddressbookService {
    @Autowired
    public AddressbookDao addressbookDao;
    @Override
    public List<Map<String, Object>> aAll(Map<String, Object> map) {
        return addressbookDao.aAll(map);
    }

    @Override
    public Integer count(Map<String, Object> map) {
        return addressbookDao.count(map);
    }

    @Override
    public Integer add(Map<String, Object> map) {
        return addressbookDao.add(map);
    }

    @Override
    public Integer update(Map<String, Object> map) {
        return addressbookDao.update(map);
    }

    @Override
    public Integer delete(int id) {
        return addressbookDao.delete(id);
    }


}
