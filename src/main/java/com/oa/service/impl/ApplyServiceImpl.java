package com.oa.service.impl;

import com.oa.dao.ApplyDao;
import com.oa.entity.BecomeForm;
import com.oa.service.ApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author Mr.suho
 * @Date 2021/12/6 9:10
 * @Description ApplyServiceImpl
 * @Version 1.0
 */
@Service
public class ApplyServiceImpl implements ApplyService {
    @Resource
    ApplyDao applyDao;

    @Override
    public Integer addMyapplyUserId(Integer userId) {
        return applyDao.addMyapplyUserId(userId);
    }

    @Override
    public List<BecomeForm> getMyapplyByUserId(Integer id) {
        return applyDao.getMyapplyByUserId(id);
    }
}
