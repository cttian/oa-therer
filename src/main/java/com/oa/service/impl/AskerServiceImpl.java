package com.oa.service.impl;

import com.oa.dao.AskerDao;
import com.oa.entity.Asker;
import com.oa.service.AskerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


//签到服务层接口是实现类
@Service
public class AskerServiceImpl implements AskerService{
	@Autowired
	private AskerDao askerDao;

	@Override
	public List<Map<String, Object>> getAllAsker(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return askerDao.getAllAsker(m);
	}

	@Override
	public Integer getAllAskerCount(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return askerDao.getAllAskerCount(m);
	}

	@Override
	public Integer add(Asker asker) {
		// TODO Auto-generated method stub
		return askerDao.add(asker);
	}

	@Override
	public Integer update(Asker asker) {
		// TODO Auto-generated method stub
		return askerDao.update(asker);
	}

	@Override
	public Integer askerYesNo(String loginName) {
		// TODO Auto-generated method stub
		return askerDao.askerYesNo(loginName);
	}

	@Override
	public Integer askerYesNoAsker(Asker asker) {
		// TODO Auto-generated method stub
		return askerDao.askerYesNoAsker(asker);
	}

	@Override
	public String askerTime(Asker asker) {
		// TODO Auto-generated method stub
		return askerDao.askerTime(asker);
	}

	@Override
	public Integer askerOutTime(Asker asker) {
		// TODO Auto-generated method stub
		return askerDao.askerOutTime(asker);
	}
//
//	@Override
//	public Integer updateWeight(Asker asker) {
//		// TODO Auto-generated method stub
//		return askerDao.updateWeight(asker);
//	}

}
