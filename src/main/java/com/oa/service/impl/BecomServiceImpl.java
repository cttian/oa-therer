package com.oa.service.impl;

import com.oa.dao.BecomeDao;
import com.oa.entity.BecomeForm;
import com.oa.service.BecomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author Mr.suho
 * @Date 2021/12/5 17:01
 * @Description BecomServiceImpl
 * @Version 1.0
 */
@Service
public class BecomServiceImpl implements BecomeService {
    @Resource
    BecomeDao becomeDao;
    @Override
    public Integer addBecomeForm(BecomeForm becomeForm) {
        return becomeDao.addBecomeForm(becomeForm);
    }
}
