package com.oa.service.impl;

import com.oa.dao.DocumentDao;
import com.oa.dao.DocumentapprovalDao;
import com.oa.service.DocumentService;
import com.sun.org.apache.xml.internal.resolver.helpers.PublicId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
//文档接口实现类
@Service
public class DocumentServiceImpl implements DocumentService {
    @Autowired
     DocumentDao documentDao;


    @Override
    public List<Map<String, Object>> doAll(Map<String, Object> map) {
        return documentDao.doAll(map);
    }

    @Override
    public Integer docount(Map<String, Object> map) {
        return documentDao.docount(map);
    }

    @Override
    public Integer doadd(Map<String, Object> map) {
        return documentDao.doadd(map);
    }

    @Override
    public Integer doupdate(Map<String, Object> map) {
        return documentDao.doupdate(map);
    }

    @Override
    public Integer dodelete(int doid) {


        return documentDao.dodelete(doid);
    }
}
