package com.oa.service.impl;


import com.oa.dao.DocumentDao;
import com.oa.dao.DocumentapprovalDao;
import com.oa.dao.DocumentrequestDao;
import com.oa.service.DocumentapprovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

//文档申批接口实现类
@Service
public class DocumentapprovalServiceImpl implements DocumentapprovalService{
    @Autowired
    DocumentapprovalDao documentapprovalDao;

    @Override
    public List<Map<String, Object>> daAll(Map<String, Object> map) {
        return documentapprovalDao.daAll(map);
    }

    @Override
    public Integer dacount(Map<String, Object> map) {
        return documentapprovalDao.dacount(map);
    }

    @Override
    public Integer daadd(Map<String, Object> map) {
        return documentapprovalDao.daadd(map);
    }

    @Override
    public Integer daupdate(Map<String, Object> map) {


        return documentapprovalDao.daupdate(map);
    }


}
