package com.oa.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.oa.service.DocumentrequestService;

import com.oa.dao.DocumentapprovalDao;
import com.oa.dao.DocumentrequestDao;

import javax.annotation.Resource;
import java.util.Map;

//文档申请接口实现类
@Service
public class DocumentrequestServiceImpl implements DocumentrequestService{
    @Resource
    DocumentrequestDao documentrequestDao;
  @Resource
  DocumentapprovalDao documentapprovalDao;
    @Override
    public Integer dradd(Map<String, Object> map) {
        int i= documentapprovalDao.daadd(map);

        int ii=documentrequestDao.dradd(map);
        return ii ;
    }

    @Override
    public Integer drupdate(Map<String, Object> map) {
        return documentrequestDao.drupdate(map);
    }
}
