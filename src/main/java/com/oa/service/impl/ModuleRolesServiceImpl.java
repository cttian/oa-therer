package com.oa.service.impl;

import com.oa.dao.ModuleRolesDao;
import com.oa.entity.ModuleRoles;
import com.oa.service.ModuleRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//模块服务层的实现类
@Service
public class ModuleRolesServiceImpl implements ModuleRolesService{
	@Autowired
	private ModuleRolesDao moduleRolesDao;

	@Override
	public Integer addModuleByIdRoles(ModuleRoles moduleRoles) {
		// TODO Auto-generated method stub
		return moduleRolesDao.addModuleByIdRoles(moduleRoles);
	}

	@Override
	public Integer removeModuleByIdRoles(ModuleRoles moduleRoles) {
		// TODO Auto-generated method stub
		return moduleRolesDao.removeModuleByIdRoles(moduleRoles);
	}

	@Override
	public Integer deleteRoleId(Integer roleId) {
		// TODO Auto-generated method stub
		return moduleRolesDao.deleteRoleId(roleId);
	}

	@Override
	public Integer selectRoleId(Integer roleId) {
		// TODO Auto-generated method stub
		return moduleRolesDao.selectRoleId(roleId);
	}
	
}
