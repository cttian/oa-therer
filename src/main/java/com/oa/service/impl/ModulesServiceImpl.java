package com.oa.service.impl;

import com.oa.dao.ModulesDao;
import com.oa.entity.Modules;
import com.oa.service.ModulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


//服务层的实现类
@Service
public class ModulesServiceImpl implements ModulesService {
	@Autowired
	private ModulesDao modulesDao;

	@Override
	public List<Map<String, Object>> getAllModules(int roleId) {
		// TODO Auto-generated method stub
		return modulesDao.getAllModules(roleId);
	}

	@Override
	public List<Map<String, Object>> getAllModulesAll(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return modulesDao.getAllModulesAll(m);
	}

	@Override
	public Integer getAllModulesAllCount(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return modulesDao.getAllModulesAllCount(m);
	}

	@Override
	public Integer add(Modules modules) {
		// TODO Auto-generated method stub
		return modulesDao.add(modules);
	}

	@Override
	public Integer update(Modules modules) {
		// TODO Auto-generated method stub
		return modulesDao.update(modules);
	}

	@Override
	public Integer del(int id) {
		// TODO Auto-generated method stub
		return modulesDao.del(id);
	}

	@SuppressWarnings("deprecation")
	@Override // 登录系统成功之后，如何根据当前用户id，获取用户所拥有的的菜单信息：
	public List<Modules> getAllModuless(int userId) {
		// TODO Auto-generated method stub
		List<Modules> list = new ArrayList<>();
		if (StringUtils.isEmpty(userId)) {// 判断是否为空
			list = modulesDao.getAllModulesAlls();// 为空获取所有
		} else {
			list = modulesDao.getAllModuless(userId);// 根据用户获取用户拥有的菜单
		}
		List<Modules> treeNodes = new ArrayList<Modules>();
		for (Modules treeNode : list) {// 循环菜单转换成菜单树
			if (treeNode.getParentId() == 0 || treeNode.getParentId() == null) {
				treeNodes.add(findChildren(treeNode, list));
			}
		}
		return treeNodes;
	}
//	树形组件获取所拥有的的菜单信息：，=true 勾选
	@Override
	public List<Modules> getAllModulesss(int userId) {
		// TODO Auto-generated method stub
		List<Modules> list = new ArrayList<>();
		List<Modules> listss = new ArrayList<>();
		list = modulesDao.getAllModulesAlls();// 获取所有
		for (Modules modules : listss) {
			modules.setChecked(false);
		}
		listss = modulesDao.getAllModuless(userId);// 根据用户获取用户拥有的菜单
		List<Modules> treeNodes = new ArrayList<Modules>();
		for (Modules treeNode : list) {// 循环所有菜单
//				System.out.println(treeNode.getName());
			for (Modules item : listss) {// 根据用户获取用户拥有的菜单
//				System.out.println(item.getName());
				if (treeNode.getId() == item.getId()) {
					treeNode.setChecked(true);// 根据用户获取用户拥有的菜单 去添加
				}
			}
//				System.out.println("11111111111111111111111");
		}
		for (Modules treeNode : list) {// 循环菜单转换成菜单树
			if (treeNode.getParentId() == 0 || treeNode.getParentId() == null) {
				treeNodes.add(findChildren(treeNode, list));
			}

		}
		for (Modules modules : treeNodes) {// 循环所有菜单
			if (modules != null && modules.getChildren() != null && modules.getChildren().size() > 0) {
				int i = 0;
				for (Modules modules2 : modules.getChildren()) {// 循环选中的子模块判断是否被选中
					if (modules2.getChecked() != null && modules2.getChecked() == true) {// 子模块被选中 i+1
						i += 1;
					}
				}
				if (i != modules.getChildren().size()) {// 判断是否和子模块是否全部选中
					modules.setChecked(false);
				}
				i = 0;//i初始化
			}

		}
		return treeNodes;
	}

	@Override // 树形组件
	public List<Modules> getAllModulessAll() {
		// TODO Auto-generated method stub
		List<Modules> list = new ArrayList<>();
		list = modulesDao.getAllModulesAlls();// 获取所有
		List<Modules> treeNodes = new ArrayList<Modules>();
		for (Modules treeNode : list) {// 循环菜单转换成菜单树
			if (treeNode.getParentId() == 0 || treeNode.getParentId() == null) {
				treeNodes.add(findChildren(treeNode, list));
			}
		}
		System.out.println(treeNodes);
		return treeNodes;
	}

	public Modules findChildren(Modules treeNode, List<Modules> treeList) {// 给父级菜单设置子级
		for (Modules item : treeList) {
			if (treeNode.getId() == item.getParentId()) {
				if (treeNode.getChildren() == null) {
					treeNode.setChildren(new ArrayList<Modules>());
				}
				treeNode.getChildren().add(findChildren(item, treeList));// 递推赋值
			}
		}
		return treeNode;// 返回菜单树
	}

	@Override
	public List<Modules> getAllModulesAlls() {
		// TODO Auto-generated method stub
		return modulesDao.getAllModulesAlls();
	}

	@Override
	public Integer getRoleByIdModels(int id) {
		// TODO Auto-generated method stub
		return modulesDao.getRoleByIdModels(id);
	}

	@Override
	public Integer getModelsByIdRole(int id) {
		// TODO Auto-generated method stub
		return modulesDao.getModelsByIdRole(id);
	}

	@Override
	public Integer getModelsParentId(Modules modules) {
		// TODO Auto-generated method stub
		return modulesDao.getModelsParentId(modules);
	}

	@Override
	public List<Modules> getAllParentId() {
		// TODO Auto-generated method stub
		return modulesDao.getAllParentId();
	}

	@Override
	public Modules getAll() {
		// TODO Auto-generated method stub
		return modulesDao.getAll();
	}

}
