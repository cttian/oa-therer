package com.oa.service.impl;

import com.oa.dao.NoteDao;
import com.oa.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

//便签服务层接口是实现类
@Service
public class NoteServiceImpl implements NoteService {
    @Autowired
    public NoteDao noteDao;


    @Override
    public List<Map<String, Object>> noteAll(Map<String, Object> map) {
        return noteDao.noteAll(map);
    }

    @Override
    public Integer count(Map<String, Object> map) {
        return noteDao.count(map);
    }

    @Override
    public Integer add(Map<String, Object> map) {
        return noteDao.add(map);
    }

    @Override
    public Integer update(Map<String, Object> map) {
        return noteDao.update(map);
    }

    @Override
    public Integer delete(int nid) {
        return noteDao.delete(nid);
    }
}
