package com.oa.service.impl;

import com.oa.dao.ProcessDao;

import com.oa.entity.Modules;
import com.oa.entity.Process;
import com.oa.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author Mr.suho
 * @Date 2021/12/1 11:09
 * @Description ProcessServiceImpl
 * @Version 1.0
 */
@Service
public class ProcessServiceImpl implements ProcessService {
    @Autowired
    ProcessDao processDao;

    @Override
    public List<Process> getFormById(Integer id) {
        return processDao.getFormById(id);
    }

    @Override
    public List<Process> getAllProcess() {
        return processDao.getAllProcess();
    }

    @Override
    public Integer insertProcess(Process process) {
        return processDao.insertProcess(process);
    }
}
