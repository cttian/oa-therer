package com.oa.service.impl;

import com.oa.dao.PublicNoticeDao;
import com.oa.entity.PublicNotice;
import com.oa.service.PublicNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Mr.suho
 * @Date 2021/12/7 12:01
 * @Description PublicNoticeServiceImpl
 * @Version 1.0
 */
@Service
public class PublicNoticeServiceImpl implements PublicNoticeService {
    @Autowired
    PublicNoticeDao publicNoticeDao;
    @Override
    public Integer addPublicNotice(PublicNotice publicNotice) {
        return publicNoticeDao.addPublicNotice(publicNotice);
    }
}
