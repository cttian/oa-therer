package com.oa.service.impl;

import com.oa.dao.RolesDao;
import com.oa.entity.Roles;
import com.oa.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


//角色服务层的实现类
@Service
public class RolesServiceImpl implements RolesService{
	@Autowired
	private RolesDao rolesDao;//自动注入
	@Override
	public List<Map<String, Object>> getAllRoles(int userId) {
		// TODO Auto-generated method stub
		return rolesDao.getAllRoles(userId);
	}
	@Override
	public List<Map<String, Object>> getAllRolesAll(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return rolesDao.getAllRolesAll(m);
	}
	@Override
	public Integer getAllRolesAllCount(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return rolesDao.getAllRolesAllCount(m);
	}
	@Override
	public Integer add(Roles roles) {
		// TODO Auto-generated method stub
		return rolesDao.add(roles);
	}
	@Override
	public Integer update(Roles roles) {
		// TODO Auto-generated method stub
		return rolesDao.update(roles);
	}
	@Override
	public Integer del(int id) {
		// TODO Auto-generated method stub
		return rolesDao.del(id);
	}
	@Override
	public List<Map<String, Object>> getAllRolesAlls(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return rolesDao.getAllRolesAlls(m);
	}
	@Override
	public Integer getAllRolesName(String name) {
		// TODO Auto-generated method stub
		return rolesDao.getAllRolesName(name);
	}

}
