package com.oa.service.impl;

import com.oa.dao.SectionDao;
import com.oa.entity.Section;
import com.oa.entity.Users;
import com.oa.service.SectionService;
import com.oa.vo.UserVO02;
import com.oa.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author Mr.suho
 * @Date 2021/11/23 14:35
 * @Description SectionServiceImpl
 * @Version 1.0
 */
@Service
public class SectionServiceImpl implements SectionService {
    @Autowired
    SectionDao sectionDao;

    @Override
    public List<Section> getAllSection(String sname) {
        return sectionDao.getAllSection(sname);
    }

    @Override
    public Integer delAllSection(Integer id) {
        return sectionDao.delAllSection(id);
    }

    @Override
    public List<Map<String, Object>> getAllSections() {
        return sectionDao.getAllSections();
    }

    @Override
    public List<Section> getAllSectionByUserId(Integer userId) {
        return sectionDao.getAllSectionByUserId(userId);
    }

    @Override
    public Integer getAllSectionByName(String sname) {
        return sectionDao.getAllSectionByName(sname);
    }



    @Override
    public Integer getSectionCount() {
        return sectionDao.getSectionCount();
    }

    @Override
    public Integer addSection(Section section) {
        return sectionDao.addSection(section);
    }

    @Override
    public Integer updateSection(Section section) {
        return sectionDao.updateSection(section);
    }

    @Override
    public Integer delSection(Integer id) {
        return sectionDao.delSection(id);
    }

    @Override
    public List<UserVo> userlist() {
        return sectionDao.userlist();
    }



    @Override
    public Integer selectSectionBySid(Integer id) {
        return sectionDao.selectSectionBySid(id);
    }

    @Override
    public List<UserVO02> getAllSectionBySid(Integer ids) {
        return sectionDao.getAllSectionBySid(ids);
    }
}
