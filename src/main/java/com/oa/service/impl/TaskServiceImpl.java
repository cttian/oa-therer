package com.oa.service.impl;

import com.oa.dao.TaskDao;
import com.oa.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.oa.service.TaskService;

import java.util.List;
import java.util.Map;

@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    TaskDao taskDao;
    @Override
    public List<Map<String, Object>> taAll(Map<String, Object> map) {
        return taskDao.taAll(map);
    }

    @Override
    public Integer tacount(Map<String, Object> map) {
        return taskDao.tacount(map);
    }

    @Override
    public Integer taadd(Map<String, Object> map) {
        return taskDao.taadd(map);
    }

    @Override
    public Integer taupdate(Map<String, Object> map) {
        return taskDao.taupdate(map);
    }

    @Override
    public Integer tadelete(int tid) {
        return taskDao.tadelete(tid);
    }
}
