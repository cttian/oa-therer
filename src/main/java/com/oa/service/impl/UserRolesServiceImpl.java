package com.oa.service.impl;

import com.oa.dao.UserRolesDao;
import com.oa.entity.UserRoles;
import com.oa.service.UserRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//角色和用户服务层的实现类
@Service
public class UserRolesServiceImpl implements UserRolesService{
	@Autowired
	private UserRolesDao userRolesDao;
	@Override
	public Integer addUsersByIdRoles(UserRoles userRoles) {
		// TODO Auto-generated method stub
		return userRolesDao.addUsersByIdRoles(userRoles);
	}

	@Override
	public Integer removeUsersByIdRoles(UserRoles userRoles) {
		// TODO Auto-generated method stub
		return userRolesDao.removeUsersByIdRoles(userRoles);
	}

}
