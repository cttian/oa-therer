package com.oa.service.impl;

import com.oa.dao.UserSectionDao;
import com.oa.entity.UserSection;
import com.oa.service.UserSectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//角色和用户服务层的实现类
@Service
public class UserSectionServiceImpl implements UserSectionService{
	@Autowired
	private UserSectionDao userSectionDao;

	@Override
	public Integer addUsersByIdSection(UserSection userSection) {
		return userSectionDao.addUsersByIdSection(userSection);
	}

	@Override
	public Integer removeUsersByIdSection(UserSection userSection) {
		return userSectionDao.removeUsersByIdSection(userSection);
	}
}
