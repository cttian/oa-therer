package com.oa.service.impl;

import com.oa.dao.UserDao;
import com.oa.entity.Section;
import com.oa.entity.Users;
import com.oa.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


//服务层接口的实现类
@Service
public class UsersServiceImpl implements UsersService{
//	自动注入dao层方法
	@Autowired
	private UserDao userDao;
	@Override
	public Users getByUserName(String userName) {
		// TODO Auto-generated method stub
		return userDao.getByUserName(userName);
	}
	@Override
	public Integer getBypsdWrongTime(int id) {
		// TODO Auto-generated method stub
		return userDao.getBypsdWrongTime(id);
	}
	@Override
	public Integer getBypsdWrongTimeClear(int id) {
		// TODO Auto-generated method stub
		return userDao.getBypsdWrongTimeClear(id);
	}

	@Override
	public List<Section> getSectionName() {
		return userDao.getSectionName();
	}

	@Override
	public List<Map<String, Object>> getUserById(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return userDao.getUserById(m);
	}
	@Override
	public Integer updateLastLoginTime(Users users) {
		// TODO Auto-generated method stub
		return userDao.updateLastLoginTime(users);
	}



	@Override
	public Integer getUserByIdCounnt(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return userDao.getUserByIdCounnt(m);
	}
	@Override
	public List<Map<String, Object>> getUser(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return userDao.getUser(m);
	}
	@Override
	public Integer getUserCount(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return userDao.getUserCount(m);
	}
	@Override
	public Integer add(Users users) {
		// TODO Auto-generated method stub
		return userDao.add(users);
	}
	@Override
	public Integer del(int id) {
		// TODO Auto-generated method stub
		return userDao.del(id);
	}
	@Override
	public Integer updateIsLockout(Users users) {
		// TODO Auto-generated method stub
		return userDao.updateIsLockout(users);
	}
	@Override
	public Integer updatePwd(Users users) {
		// TODO Auto-generated method stub
		return userDao.updatePwd(users);
	}

	public Integer update(Users users) {
		// TODO Auto-generated method stub
		return userDao.update(users);
	}
	@Override
	public Integer getUserByIdRole(int id) {
		// TODO Auto-generated method stub
		return userDao.getUserByIdRole(id);
	}

	@Override
	public List<Map<String, Object>> getZiXun(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return userDao.getZiXun(m);
	}
	@Override
	public Integer getZiXunCount(Map<String, Object> m) {
		// TODO Auto-generated method stub
		return userDao.getZiXunCount(m);
	}


}
