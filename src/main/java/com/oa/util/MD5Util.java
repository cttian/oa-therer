package com.oa.util;

import java.util.Random;
import org.springframework.util.DigestUtils;

/**
 *	加密工具类
 * @author CrazyCat
*/
public class MD5Util {
	
	/**
	 * 
	 * @Title: getSalt
	 * @Description: 获取随机盐
	 * @param @return 参数
	 * @return String 返回类型
	 */
	public static String getSalt() {
		StringBuilder sb = new StringBuilder(16); 
		String[] str1 = {"Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","Z","X","C","V","B","N","M"};
		String[] str2 = {"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m"};
		String[] str3 = {"1","2","3","4","5","6","7","8","9","0"};
		String[] str4 = {"!","@","#","$","%","&","*","?"};
		for(int i = 0;i<4;i++) {
			String a = str1[new Random().nextInt(str1.length)];
			String b = str2[new Random().nextInt(str2.length)];
			String c = str3[new Random().nextInt(str3.length)];
			String d = str4[new Random().nextInt(str4.length)];
			sb.append(a);
			sb.append(b);
			sb.append(c);
			sb.append(d);
		}
//		System.out.println(sb.toString());
		return sb.toString();
	}
	
	/**
	 * 
	 * @Title: generate
	 * @Description: 生成加密密文
	 * @param password 密码
	 * @param salt 盐
	 * @return String 返回类型
	 */
	public static String generate(String password, String salt){
        String p = password + salt;
//		System.out.println("p："+p);
//		System.out.println("密码"+DigestUtils.md5DigestAsHex(p.getBytes()));
		return DigestUtils.md5DigestAsHex(p.getBytes());

    }
	
	/**
	 * 
	 * @Title: verify
	 * @Description: 验证密码是否正确
	 * @param password 明文密码
	 * @param salt 盐
	 * @param old 密文密码
	 * @return boolean 返回类型
	 */
	public static boolean verify(String password, String salt, String  old) {
		String generate = generate(password, salt);
//		System.out.println("页面组合密码"+generate);
		if(generate.equals(old)) {
			return true;
		}
		return false;
	}
	
	
//	public static void main(String[] args) {
//		String salt = "Ep7@Vs7%Hk5@Ug4*";
//		String pwd = "admin1";
//		String old = "e779ddbd109869406df0744fa6d040a7";
//		boolean verify = verify(pwd, salt, old);
//		System.out.println(verify);
//
//	}
}