package com.oa.vo;

import lombok.Data;

/**
 * @Author Mr.suho
 * @Date 2021/11/24 10:20
 * @Description SectionVO
 * @Version 1.0
 */
@Data
public class SectionVO {
    private Integer id;
    private String sname;
    private Integer countsid;
}
