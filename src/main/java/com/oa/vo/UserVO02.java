package com.oa.vo;

import lombok.Data;

/**
 * @Author Mr.suho
 * @Date 2021/11/28 21:20
 * @Description UserVO02
 * @Version 1.0
 */
@Data
public class UserVO02 {
    private Integer id;
    private String loginName;
    private String protectEmail;
    private String protectMtel;
}
