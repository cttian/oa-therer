package com.oa.vo;

import lombok.Data;

/**
 * @Author Mr.suho
 * @Date 2021/11/24 14:45
 * @Description UserVo
 * @Version 1.0
 */
@Data
public class UserVo {
    /*用户表表的部门id和部门人数*/
    private Integer sid;
    private Integer countsid;

}
