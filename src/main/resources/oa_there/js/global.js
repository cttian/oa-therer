var globalData = {
	server: "http://127.0.0.1:8080/",
	pre: "http://127.0.0.1:8080/crmh5/",
	socket: "ws://localhost:8080/",
	//把登陆的用户名存入h5的缓存
	setUserInfo: function(username, token, uid) {
		window.localStorage.setItem("username", username);
		window.localStorage.setItem("token", token);
		window.localStorage.setItem("uid", uid);
	},
	setPer: function(plist) { //设置权限列表
		window.localStorage.setItem("plist", plist);
	},
	hasPer: function(pvalue) {
		return window.localStorage.getItem("plist").includes(pvalue);
	},
	//把选中的学生id入h5的缓存
	setIdsInfo: function(ids) {
		window.localStorage.setItem("ids", ids);
	},
	//将部门id存到这里
	setIdsid: function(id) {
		window.localStorage.setItem("id", id);
	},
	//从H5缓存中取出选中的学生id
	getIds: function() {
		return window.localStorage.getItem("ids");
	},
	//退出选中的学生id
	loginOutIds: function() {
		globalData.setIdsInfo(null);
	},
	//取出部门id
	getIdsid: function() {
		return window.localStorage.getItem("id");
	},
	//从H5缓存中取出当前登录的用户名
	getUserName: function() {
		return window.localStorage.getItem("username");
	},
	getUserID: function() {
		return window.localStorage.getItem("uid");
	},
	getToken: function() {
		return window.localStorage.getItem("token");
	},
	getPer: function() {
		return window.localStorage.getItem("plist");
	},
	getUid: function() {
		return window.localStorage.getItem("uid");
	},
	//退出登录
	loginOut: function() {
		globalData.setUserInfo(null, null, null);
		window.location.href = "login.html";
	}
};
var a = globalData.getToken();
if(a == null || a == "null") {
	var url = window.location.href;
	if(url.indexOf("login.html") >= 0) {

	} else {
		window.location.href = "login.html";
	}
} else {
	var url = window.location.href;
	if(url.indexOf("login.html") >= 0) {
		window.location.href = "index.html";
	}
}
